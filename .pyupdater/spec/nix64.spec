# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['/home/hp/django_projects/flaskapp/run.py'],
             pathex=['/home/hp/django_projects/flaskapp', '/home/hp/django_projects/flaskapp/.pyupdater/spec'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=['/home/hp/django_projects/pd/lib/python3.5/site-packages/pyupdater/hooks'],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='nix64',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='nix64')
