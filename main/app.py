"""
A Flask file server for demonstrating application updates with PyUpdater.
"""
import logging
from flask import Flask

LOCALHOST = '127.0.0.1'

logger = logging.getLogger(__name__)


def startapp():
    """
    Run a Flask file server on the given port.

    Explicitly specify instance_path, because Flask's
    auto_find_instance_path can fail when run in a frozen app.
    """
    app = Flask(__name__)

    @app.route('/', methods=['GET'])
    def FileserverIsReady():  # pylint: disable=unused-variable
        """
        Used to test if file server has started.
        """
        return 'version 0.0.5'


    app.run(host=LOCALHOST, port=5000)
