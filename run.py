"""
run.py

PyUpdaterWxDemo can be launched by running "python run.py"
"""
import logging
import os
import sys
import threading
import time
import argparse
from threading import Timer


from pyupdater.client import Client

import main
from main.fileserver import RunFileServer
from main.fileserver import WaitForFileServerToStart
from main.fileserver import ShutDownFileServer
from client_config import ClientConfig
from main.app import startapp

logger = logging.getLogger(__name__)


def StartFileServer():
	"""
	Start file server.
	"""
	#fileServerDir = os.environ.get('PYUPDATER_FILESERVER_DIR')
	# if not fileServerDir:
	# 	message = \
	# 		"The PYUPDATER_FILESERVER_DIR environment variable is not set."
	# 	logger.error(message)
	# else:
	fileServerDir = '/home/hp/django_projects/flaskapp/pyu-data/deploy'
	fileServerPort = 8000
	thread = threading.Thread(target=RunFileServer,
							  args=(fileServerDir, fileServerPort))
	thread.start()
	WaitForFileServerToStart(fileServerPort)
	return fileServerPort


def CheckForUpdates():
	"""
	Check for updates.

	Channel options are stable, beta & alpha
	Patches are only created & applied on the stable channel
	"""
	CLIENT_CONFIG = ClientConfig()
	client = Client(CLIENT_CONFIG, refresh=True)
	appUpdate = client.update_check(CLIENT_CONFIG.APP_NAME,main.__version__,channel='stable')
	if appUpdate:
		downloaded = appUpdate.download()
		if downloaded:
			ShutDownFileServer(8000)
			logger.error('Extracting update and restarting...')
			time.sleep(10)
			appUpdate.extract_restart()
	else:
		logger.error('No update found')
	Timer(10.0, CheckForUpdates).start()



def Run():
	"""
	The main entry point.
	"""
	StartFileServer()
	Timer(1.0, CheckForUpdates).start()
	startapp()


if __name__ == "__main__":
	Run()
